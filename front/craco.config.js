const CracoAlias = require("craco-alias");

module.exports = {
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        baseUrl: "./",
        aliases: {
          Test: "./src/Test",
          application: "./src/application",
          configs: "./src/configs",
        }
      }
    }
  ]
};