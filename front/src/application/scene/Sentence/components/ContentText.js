import { styled } from "@material-ui/core"

const ContentText = styled("div")({
  display: "flex",
  justifyContent: "flex-end",
  marginTop: 10,
})

export default ContentText