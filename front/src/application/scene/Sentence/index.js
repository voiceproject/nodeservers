import { Table, TableBody, TableCell, TableHead, TableRow , TextField } from "@material-ui/core"
import { NavBar, Link, Button, Content } from "application/components"
import config from "configs/config.json"
import React from "react"
import axios from "axios"

import { ContentText } from "./components"

export default function Sentence() {
  const [sentences, setSentences] = React.useState([])
  const [search, setSearch] = React.useState("")

  async function getAllSentences() {
    let allSentences

    try {
      allSentences = await axios.get(`${config.server}/sentences?search=${search}`)
    } catch (e) {
      return
    }

    setSentences(allSentences.data.items)
  }

  function changeSearch(e) {
    setSearch(e.target.value)
  }

  React.useEffect(() => {
    getAllSentences() // eslint-disable-next-line
  }, [search])

  return (
    <NavBar title="Toutes les phrases enregistrées">
      <ContentText>
        <TextField 
            label="Recherche"
            value={search}
            onChange={changeSearch}
          />
      </ContentText>
      <Content>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="center">Phrase en base</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sentences.map(sentence => (
            <TableRow key={sentence._id}>
              <TableCell>{sentence.sentence}</TableCell>
            </TableRow>
          ))}
          </TableBody>
        </Table>
      </Content>
      <Content marginlow="true">
        <Button>
          <Link to="/">
            Retour
          </Link>
        </Button>
      </Content>
    </NavBar>
  )
}