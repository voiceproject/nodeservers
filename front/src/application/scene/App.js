import { sentences, dashboard } from "configs/routes"
import { Dashboard, Sentence } from "application/scene"
import { Route, Switch } from "react-router-dom"
import React from "react"

export default function App() {
  return (
    <Switch>
      <Route exact path={dashboard} component={Dashboard} />
      <Route path={sentences} component={Sentence} />
    </Switch>
  )
}