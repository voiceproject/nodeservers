import { NavBar, Link, Button, Content } from "application/components"
import config from "configs/config.json"
import React from "react"

import { TitleSentence, Sentence } from "./components"

export default function Dashboard() {
  const [sentence, setSentence] = React.useState("");
  const source = new EventSource(`${config.server}/sentences/stream`);

  function connectSse() {
    source.onmessage = (data) => {
      setSentence(data.data);
    };
  }

  async function closeSse() {
    source.close();
  }

  React.useEffect(() => {
    connectSse(); // eslint-disable-next-line
  }, []);

  return (
    <NavBar title="Voice project - groupe 4">
      <Content>
        <TitleSentence>
          Dernière phrase:
        </TitleSentence>
        <br /> 
      </Content>
      <Content marginhigh="true">
        <Sentence>{sentence}</Sentence>
      </Content>
      <Content margintop="true">
        <Button>
          <Link to="/sentences" onClick={closeSse}>
            Toutes les phrases
          </Link>
        </Button>
      </Content>
    </NavBar>
  );
}
