import { styled } from "@material-ui/styles"

const TitleSentence = styled("span")({
  fontSize: 50,
  textDecoration: "underline",
  fontWeight: "bold"
})

export default TitleSentence