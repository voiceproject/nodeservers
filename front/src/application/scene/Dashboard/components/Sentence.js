import { styled } from "@material-ui/styles"

const TitleSentence = styled("span")({
  fontSize: 30,
})

export default TitleSentence