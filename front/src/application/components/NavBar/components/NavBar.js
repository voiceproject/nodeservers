import { styled } from "@material-ui/styles"
import { AppBar } from "@material-ui/core"

const NavBar = styled(AppBar)({
  height: 50,
  background: "#EEBF0D"
})

export default NavBar