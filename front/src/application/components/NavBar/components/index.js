export { default as NavBarComponent } from "./NavBar"
export { default as Content } from "./Content"
export { default as Title } from "./Title"