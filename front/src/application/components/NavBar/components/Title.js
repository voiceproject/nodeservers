import { styled } from "@material-ui/styles"

const Title = styled("span")({
  textDecoration: "underline",
  textAlign: "center",
  fontSize: 30,
  marginTop: 7,
})

export default Title