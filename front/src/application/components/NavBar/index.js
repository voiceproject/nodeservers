import { NavBarComponent, Content, Title } from "./components"
import React from "react"

export default function NavBar({ children, title }) {
  return(
    <>
      <NavBarComponent>
        <Title>{title}</Title>
      </NavBarComponent>
      <Content>
        {children}
      </Content>
    </>
  )
}