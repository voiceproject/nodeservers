import { Button as ButtonComponent } from "@material-ui/core"
import { styled } from "@material-ui/styles"

const Button = styled(ButtonComponent)({
  background: "#EEBF0D"
})

export default Button