import { ButtonComponent } from "./components"
import React from "react"

export default function Button({ children }) {
  return(
    <ButtonComponent>
      {children}
    </ButtonComponent>
  )
} 