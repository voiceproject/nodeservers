import { Link as LinkComponent } from "react-router-dom"
import { styled } from "@material-ui/styles"

const Link = styled(LinkComponent)({
  textDecoration: "none",
  color: "black",
})

export default Link