import { LinkComponent } from "./components"
import React from "react"

export default function Link({ to, onClick, children }) {
  return(
    <LinkComponent
      to={to}
      onClick={onClick}
    >
      {children}
    </LinkComponent>
  )
}