import { styled } from "@material-ui/core"

const Content = styled("div")(({ marginhigh, marginlow }) => {
  let style = {
    display: "flex",
    justifyContent: "center"
  }
  
  if(marginhigh === "true") {
    style.marginTop = 100
  }

  if(marginlow === "true") {
    style.marginTop = 50
  }

  return style
})

export default Content