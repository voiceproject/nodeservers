import { audioModel } from "./model"

export function newFileAudio(name, path) {
  const AudioModel = audioModel
  let newFile

  try {
    newFile = new AudioModel({ name, path }).save()
  } catch (e) {
    console.log("Error pending add new track audio inside db", e)
  }

  return newFile
}

export function getAllTrack() {
  return audioModel.find()
}

export function oneTrack(id) {
  return audioModel.find({ _id: id })
}