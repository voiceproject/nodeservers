import { addNew, getTrack, allTrack } from "./action"
import KoaRouter from "koa-router"

const route = new KoaRouter()

route.get("/", allTrack)
route.get("/:id", getTrack)
route.post("/", addNew)

export default route