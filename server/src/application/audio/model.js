import mongoose from "mongoose"

const audio = new mongoose.Schema({
  name: String,
  path: String
})

export const audioModel = mongoose.model("audio", audio) 