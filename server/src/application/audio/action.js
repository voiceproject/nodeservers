import { newFileAudio, getAllTrack, oneTrack } from "./service"

export async function addNew(ctx) {
  const { path, name } = ctx.request.files.track
  let newTrack

  try {
    newTrack = await newFileAudio(name, path)
  } catch (e) {
    console.log("Error execute newFileAudio", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: newTrack
  }
}

export async function allTrack(ctx) {
  let allItems

  try {
    allItems = await getAllTrack()
  } catch (e) {
    console.log("Error pending return all track")
    ctx.body = 500
    return
  }

  ctx.body = {
    items: allItems
  }
}

export async function getTrack(ctx) {
  const{ id } = ctx.params
  let track

  try {
    track = await oneTrack(id)
  } catch (e) {
    console.log("Error pending return one track")
    ctx.body = 500
    return
  }

  ctx.body = {
    items: track
  }
}