import { sentenceModel } from "application/sentences/model"

export default function getOneSentence(id) {
    return sentenceModel.find({ _id: id })
}
  
