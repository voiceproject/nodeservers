import { sentenceModel } from "application/sentences/model"

export default function addSentence(sentence) {
  const NewSentence = sentenceModel
  let addWord
  
  try {
    addWord = new NewSentence({ sentence }).save()
  } catch (e) {
    console.log("Error pending add sentence inside db", e)
    return
  }
  
  return addWord
}