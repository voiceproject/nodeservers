export { default as getAllSentences } from "./getAllSentences"
export { default as getOneSentence } from "./getOneSentence"
export { default as addSentence } from "./addSentence"