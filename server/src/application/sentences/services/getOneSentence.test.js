import { sentenceModel } from "application/sentences/model"

import getOneSentence from "./getOneSentence"

describe("Get all sentences and return", () => {
    it("no search", async () => {
        let result = [
            {
                "_id": "5ea2adf627daf24d818899e2",
                "sentence": "test",
                "__v": 0
            }]

        sentenceModel.find = jest.fn().mockReturnValueOnce(Promise.resolve(result))
        const exec = await getOneSentence("5ea2adf627daf24d818899e2")

        expect(exec).toStrictEqual(result)
    })
})