import { sentenceModel } from "application/sentences/model"

export default async function getAllSentences(search) {
  const conditionSearch = {}
  
  if(search !== undefined){
     conditionSearch.sentence = new RegExp(search, "i")
  }
  
  return await sentenceModel.find(conditionSearch)
}
