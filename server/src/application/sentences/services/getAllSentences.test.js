import { sentenceModel } from "application/sentences/model"

import getAllSentences from "./getAllSentences"

describe("Get all sentences and return", () => {
    it("no search", async () => {
        let result = [
            {
                "_id": "5ea2adf627daf24d818899e2",
                "sentence": "test",
                "__v": 0
            }]

        sentenceModel.find = jest.fn().mockReturnValueOnce(Promise.resolve(result))
        const exec = await getAllSentences()

        expect(exec).toStrictEqual(result)
    })

    it("with search", async () => {
        let result = [
            {
                "_id": "5ea2adf627daf24d818899e2",
                "sentence": "test",
                "__v": 0
            }]

        sentenceModel.find = jest.fn().mockReturnValueOnce(Promise.resolve(result))
        const exec = await getAllSentences("test")

        expect(exec).toStrictEqual(result)
    })
})