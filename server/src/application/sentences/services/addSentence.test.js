import { sentenceModel } from "application/sentences/model"

import addSentence from "./addSentence"

describe("Add a sentence inside db", () => {
    let resultFunc = { "_id": "5ea2adf627daf24d818899e2", "sentence": "test" }
    let sentence = "test"

    it("Success add", async () => {
        jest.spyOn(sentenceModel.prototype, "save").mockReturnValueOnce(Promise.resolve(resultFunc))

        let result = await addSentence(sentence)

        expect(result).toStrictEqual(resultFunc)
    })
})