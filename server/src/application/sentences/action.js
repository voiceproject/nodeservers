import { getAllSentences, getOneSentence, addSentence } from "./services"

let createSentence = ""

export async function allSentences(ctx) {
  const { search } = ctx.request.query
  let all

  try {
    all = await getAllSentences(search)
  } catch (e) {
    console.log("Error pending get all sentences", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: all,
  }
}

export async function oneSentence(ctx) {
  const { id } = ctx.params
  let oneSentence

  try {
    oneSentence = await getOneSentence(id)
  } catch (e) {
    console.log("Error pending get one sentence", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: oneSentence,
  }
}

export async function create(ctx) {
  const { sentence } = ctx.request.body
  let newSentence

  try {
    newSentence = await addSentence(sentence)
  } catch (e) {
    console.log("Error pending call addSentence", e)
    ctx.status = 500
    return
  }

  createSentence = newSentence.sentence

  ctx.body = {
    items: newSentence,
  }
}

export function stream(ctx) {
  ctx.status = 200
  ctx.set({
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    Connection: "Keep-Alive",
  })

  if (createSentence === "") {
    ctx.res.write(``)
  } else {
    ctx.res.write(`data: ${createSentence}\n\n`)
  }

  createSentence = ""
}
