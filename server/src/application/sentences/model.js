import mongoose from "mongoose"

const sentences = new mongoose.Schema({
  sentence: String
})

export const sentenceModel = mongoose.model("sentences", sentences)