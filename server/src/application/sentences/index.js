import { allSentences, create, oneSentence, stream } from "./action"
import KoaRouter from "koa-router"

const route = new KoaRouter()

route.get("/stream", stream)
route.get("/", allSentences)
route.get("/:id", oneSentence)
route.post("/", create)

export default route