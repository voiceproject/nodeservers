import config from "configs/config.json"
import bootstrap from "./bootstrap"
import mongoose from "mongoose"
import helmet from "koa-helmet"
import koaBody from "koa-body"
import serve from "koa-static"
import mount from "koa-mount"
import cors from "@koa/cors"
import eTag from "koa-etag"
import Koa from "koa"

const { port, mongo } = config.global
const app = new Koa()

app.use(mount("/audio", serve(`${__dirname}/../../files/audio`)))
app.use(cors())
app.use(helmet())
app.use(eTag())

app.use(koaBody({
  multipart: true,
  formidable: {
    uploadDir: "files/audio",
    keepExtensions: true
  }
}))

mongoose.connect(mongo.connect, { 
  useNewUrlParser: true, 
  useUnifiedTopology: true, 
})

app.listen(port)

const mainRouter = bootstrap()
app.use(mainRouter.routes())
app.use(mainRouter.allowedMethods())