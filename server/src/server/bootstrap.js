import KoaRouter from "koa-router"

export default () => {
  const route = new KoaRouter()

  const { sentences, audio } = require("application")

  route.use("/sentences", sentences.routes(), sentences.allowedMethods())
  route.use("/audio", audio.routes(), audio.allowedMethods())

  route.use(ctx => {
    ctx.status = 404
  })

  return route
}

