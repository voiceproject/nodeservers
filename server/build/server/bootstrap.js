"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = () => {
  const route = new _koaRouter.default();

  const {
    sentences,
    audio
  } = require("../application");

  route.use("/sentences", sentences.routes(), sentences.allowedMethods());
  route.use("/audio", audio.routes(), audio.allowedMethods());
  route.use(ctx => {
    ctx.status = 404;
  });
  return route;
};

exports.default = _default;