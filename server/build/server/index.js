"use strict";

var _config = _interopRequireDefault(require("../../configs/config.json"));

var _bootstrap = _interopRequireDefault(require("./bootstrap"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _koaHelmet = _interopRequireDefault(require("koa-helmet"));

var _koaBody = _interopRequireDefault(require("koa-body"));

var _koaStatic = _interopRequireDefault(require("koa-static"));

var _koaMount = _interopRequireDefault(require("koa-mount"));

var _cors = _interopRequireDefault(require("@koa/cors"));

var _koaEtag = _interopRequireDefault(require("koa-etag"));

var _koa = _interopRequireDefault(require("koa"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  port,
  mongo
} = _config.default.global;
const app = new _koa.default();
app.use((0, _koaMount.default)("/audio", (0, _koaStatic.default)(`${__dirname}/../../files/audio`)));
app.use((0, _cors.default)());
app.use((0, _koaHelmet.default)());
app.use((0, _koaEtag.default)());
app.use((0, _koaBody.default)({
  multipart: true,
  formidable: {
    uploadDir: "files/audio",
    keepExtensions: true
  }
}));

_mongoose.default.connect(mongo.connect, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

app.listen(port);
const mainRouter = (0, _bootstrap.default)();
app.use(mainRouter.routes());
app.use(mainRouter.allowedMethods());