"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sentenceModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const sentences = new _mongoose.default.Schema({
  sentence: String
});

const sentenceModel = _mongoose.default.model("sentences", sentences);

exports.sentenceModel = sentenceModel;