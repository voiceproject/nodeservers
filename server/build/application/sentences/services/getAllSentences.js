"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getAllSentences;

var _model = require("../model");

async function getAllSentences(search) {
  const conditionSearch = {};

  if (search !== undefined) {
    conditionSearch.sentence = new RegExp(search, "i");
  }

  return await _model.sentenceModel.find(conditionSearch);
}