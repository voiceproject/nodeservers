"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "getAllSentences", {
  enumerable: true,
  get: function () {
    return _getAllSentences.default;
  }
});
Object.defineProperty(exports, "getOneSentence", {
  enumerable: true,
  get: function () {
    return _getOneSentence.default;
  }
});
Object.defineProperty(exports, "addSentence", {
  enumerable: true,
  get: function () {
    return _addSentence.default;
  }
});

var _getAllSentences = _interopRequireDefault(require("./getAllSentences"));

var _getOneSentence = _interopRequireDefault(require("./getOneSentence"));

var _addSentence = _interopRequireDefault(require("./addSentence"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }