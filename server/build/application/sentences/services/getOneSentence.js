"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getOneSentence;

var _model = require("../model");

function getOneSentence(id) {
  return _model.sentenceModel.find({
    _id: id
  });
}