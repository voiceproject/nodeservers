"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = addSentence;

var _model = require("../model");

function addSentence(sentence) {
  const NewSentence = _model.sentenceModel;
  let addWord;

  try {
    addWord = new NewSentence({
      sentence
    }).save();
  } catch (e) {
    console.log("Error pending add sentence inside db", e);
    return;
  }

  return addWord;
}