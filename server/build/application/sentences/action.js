"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.allSentences = allSentences;
exports.oneSentence = oneSentence;
exports.create = create;
exports.stream = stream;

var _services = require("./services");

let createSentence = "";

async function allSentences(ctx) {
  const {
    search
  } = ctx.request.query;
  let all;

  try {
    all = await (0, _services.getAllSentences)(search);
  } catch (e) {
    console.log("Error pending get all sentences", e);
    ctx.status = 500;
    return;
  }

  ctx.body = {
    items: all
  };
}

async function oneSentence(ctx) {
  const {
    id
  } = ctx.params;
  let oneSentence;

  try {
    oneSentence = await (0, _services.getOneSentence)(id);
  } catch (e) {
    console.log("Error pending get one sentence", e);
    ctx.status = 500;
    return;
  }

  ctx.body = {
    items: oneSentence
  };
}

async function create(ctx) {
  const {
    sentence
  } = ctx.request.body;
  let newSentence;

  try {
    newSentence = await (0, _services.addSentence)(sentence);
  } catch (e) {
    console.log("Error pending call addSentence", e);
    ctx.status = 500;
    return;
  }

  createSentence = newSentence.sentence;
  ctx.body = {
    items: newSentence
  };
}

function stream(ctx) {
  ctx.status = 200;
  ctx.set({
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    Connection: "Keep-Alive"
  });

  if (createSentence === "") {
    ctx.res.write(``);
  } else {
    ctx.res.write(`data: ${createSentence}\n\n`);
  }

  createSentence = "";
}