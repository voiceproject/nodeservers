"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _action = require("./action");

var _koaRouter = _interopRequireDefault(require("koa-router"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const route = new _koaRouter.default();
route.get("/stream", _action.stream);
route.get("/", _action.allSentences);
route.get("/:id", _action.oneSentence);
route.post("/", _action.create);
var _default = route;
exports.default = _default;