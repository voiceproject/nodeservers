"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addNew = addNew;
exports.allTrack = allTrack;
exports.getTrack = getTrack;

var _service = require("./service");

async function addNew(ctx) {
  const {
    path,
    name
  } = ctx.request.files.track;
  let newTrack;

  try {
    newTrack = await (0, _service.newFileAudio)(name, path);
  } catch (e) {
    console.log("Error execute newFileAudio", e);
    ctx.status = 500;
    return;
  }

  ctx.body = {
    items: newTrack
  };
}

async function allTrack(ctx) {
  let allItems;

  try {
    allItems = await (0, _service.getAllTrack)();
  } catch (e) {
    console.log("Error pending return all track");
    ctx.body = 500;
    return;
  }

  ctx.body = {
    items: allItems
  };
}

async function getTrack(ctx) {
  const {
    id
  } = ctx.params;
  let track;

  try {
    track = await (0, _service.oneTrack)(id);
  } catch (e) {
    console.log("Error pending return one track");
    ctx.body = 500;
    return;
  }

  ctx.body = {
    items: track
  };
}