"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.audioModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const audio = new _mongoose.default.Schema({
  name: String,
  path: String
});

const audioModel = _mongoose.default.model("audio", audio);

exports.audioModel = audioModel;