"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newFileAudio = newFileAudio;
exports.getAllTrack = getAllTrack;
exports.oneTrack = oneTrack;

var _model = require("./model");

function newFileAudio(name, path) {
  const AudioModel = _model.audioModel;
  let newFile;

  try {
    newFile = new AudioModel({
      name,
      path
    }).save();
  } catch (e) {
    console.log("Error pending add new track audio inside db", e);
  }

  return newFile;
}

function getAllTrack() {
  return _model.audioModel.find();
}

function oneTrack(id) {
  return _model.audioModel.find({
    _id: id
  });
}