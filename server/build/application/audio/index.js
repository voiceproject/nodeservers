"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _action = require("./action");

var _koaRouter = _interopRequireDefault(require("koa-router"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const route = new _koaRouter.default();
route.get("/", _action.allTrack);
route.get("/:id", _action.getTrack);
route.post("/", _action.addNew);
var _default = route;
exports.default = _default;