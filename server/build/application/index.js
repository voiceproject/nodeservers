"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "sentences", {
  enumerable: true,
  get: function () {
    return _sentences.default;
  }
});
Object.defineProperty(exports, "audio", {
  enumerable: true,
  get: function () {
    return _audio.default;
  }
});

var _sentences = _interopRequireDefault(require("./sentences"));

var _audio = _interopRequireDefault(require("./audio"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }